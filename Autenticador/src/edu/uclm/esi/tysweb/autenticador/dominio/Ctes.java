package edu.uclm.esi.tysweb.autenticador.dominio;

public class Ctes {
	public static final String consumerKey = "consumerKey";
	public static final String consumerSecret = "consumerSecret";
	public static final String token = "token";
	public static final String callback = "callback";
	public static final String aplicacion = "aplicacion";
}
